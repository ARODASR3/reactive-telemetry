/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ratella.ReactiveReadingListWebApp.Client;

import com.ratella.ReactiveReadingListWebApp.model.Telemetry;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

/**
 *
 * @author aleksei.escobar
 */
public class WebClientExample {
    public static void main(String[] args) throws InterruptedException {
        WebClient client = WebClient.create("http://localhost:8080");
        Flux<Telemetry> bookFlux = client.get()
  .uri("/telemetry/all")
  .retrieve()
  .bodyToFlux(Telemetry.class);
         
    bookFlux.subscribe(System.out::println);
        Thread.sleep(3000);
    }
 
}
