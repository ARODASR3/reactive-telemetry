/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ratella.ReactiveReadingListWebApp.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author aleksei.escobar
 */
@Document(collection = "telemetry")
public class Telemetry {
    @Id
  
    private String messageId;
    private String temperature;
    private String humidity;

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }
    
    	@Override
	public String toString() {
		return "Telemetry{" +
				"messageId='" + messageId + '\'' +
				", temperature='" + temperature + '\'' +
				", humdidty='" + humidity + '\'' +
				'}';
	}
}
