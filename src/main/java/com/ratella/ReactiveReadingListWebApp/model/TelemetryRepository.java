package com.ratella.ReactiveReadingListWebApp.model;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface TelemetryRepository extends ReactiveMongoRepository<Telemetry, String> {

    //public Flux<Telemetry> findById(int id);

}
