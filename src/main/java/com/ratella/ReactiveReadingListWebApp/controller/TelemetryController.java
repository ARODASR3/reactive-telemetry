/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ratella.ReactiveReadingListWebApp.controller;

import com.ratella.ReactiveReadingListWebApp.model.Telemetry;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.EmitterProcessor;
import reactor.core.publisher.Flux;
import com.ratella.ReactiveReadingListWebApp.model.TelemetryRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

/**
 *
 * @author aleksei.escobar
 */
@CrossOrigin(value = { "http://localhost:4200"},allowedHeaders = { "Access-Control-Allow-Origin" },
  maxAge = 900)
@RestController
@RequestMapping("/telemetry")
public class TelemetryController {
    private TelemetryRepository readingListRepository;
    private EmitterProcessor<Telemetry> notificationProcessor;
    
    @PostConstruct
    private void createProcessor() {
        notificationProcessor = EmitterProcessor.<Telemetry>create();
    }
    
     @Autowired
  public TelemetryController(
      TelemetryRepository readingListRepository) {
    this.readingListRepository = readingListRepository;
  }
    
    @GetMapping(
        value = "/all",
        produces = "application/json")
    public Flux<Telemetry>getAll() {
        Flux<Telemetry> readingList = readingListRepository.findAll();
       return readingList;
    }
}
