package com.ratella.ReactiveReadingListWebApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.ratella"})
public class ReactiveTelemetryWebAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReactiveTelemetryWebAppApplication.class, args);
	}

}
